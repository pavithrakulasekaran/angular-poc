import { Component, OnInit, Input } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserserviceService } from 'src/shared/services/userservice.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { user } from 'src/shared/model/user';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userForm: FormGroup;
  user: user = new user();
  UserValues: user[] = [];
  temparray: user[] = [];
  search:string;
  field:string[]=["Id,LastName,UserName,EmailId,Project,Role"]
  order:number=1
  Size:number;
  current=0;
  index=1;
  check:boolean
  constructor(private modal: NgbModal,
    private userservice: UserserviceService,
    private fb: FormBuilder) {
      
    this.userservice.GetUsers().subscribe(
      data => {this.UserValues = data
   this.myfunction(this.index)
     
       
      }
    )
    this.Size=2;
  }

  ngOnInit() {

    this.userForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      userName: ['', [Validators.required, Validators.minLength(3)]],
      emailId: ['', [Validators.required, Validators.email]],
      projects: [" ", [Validators.required]],
      role: ["", [Validators.required]]
    })
       this.temparray=this.UserValues
 
    
  }



  AddUserModal(content) {

    this.user = new user();
    this.modal.open(content);

  }
  edit(content, data) {
    this.modal.open(content);
    this.userservice.GetUser(data.id).subscribe(
      data => {
      this.user = data
      })


  }
  save() {
  
    if (this.user.id == undefined) {
      this.userservice.SaveUser(this.user).subscribe(
        data => {
          this.UserValues.push(data)
          this.userForm.reset()
          window.location.reload();
        })
    }
    else {
      this.userservice.UpdateUser(this.user).subscribe(
        data => {
          this.UserValues.splice(this.user.id, 1, data)
          window.location.reload();

        }
      )
    }
  }
  DeleteUserData(user) {
 let result=confirm("Want to delete data permanently?") 
 if(result==true){
   window.location.reload()
     this.userservice.DeleteUser(user).subscribe(data=>{
      
      console.log(this.UserValues) }
    ) 
      }
  }
  sortTable(prop: string) {
    this.field = [prop]
    this.order = this.order * (-1); 
    return false; 
  }
  myfunction(event){
    this.check=false;
 this.userservice.GetUsers().subscribe(
  data => {this.UserValues = data
  })
   this.current=(event-1)*this.Size;
        this.temparray=this.UserValues.splice(this.current,this.Size)  
     }



     ChangeDataSize(event){ 
      this.Size=event.target.value;
        this.myfunction(this.index)
        this.check=true;
    }
   
  }




