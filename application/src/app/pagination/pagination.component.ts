import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserserviceService } from 'src/shared/services/userservice.service';
import { user } from 'src/shared/model/user';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  employee: user[]
  length: number
  count = [];
  no_of_pages: number;
  isactive: number = 1;
@Input() check:boolean
  @Input() size: number;
  @Output() Value = new EventEmitter();

  constructor(private userservice: UserserviceService) { }

  ngOnInit() {
    this.userservice.GetUsers().subscribe(data => {
    this.employee = data
      this.length = this.employee.length;
 
    })
    
  }
  ngDoCheck(){
   this.count=[];
    this.no_of_pages = Math.ceil(this.length / this.size)
    for (let i = 1; i <= this.no_of_pages; i++) {
      this.count.push(i)
    }
    if(!this.count.includes(this.isactive) || this.check==true ){
      this.isactive=1
    }
  }

  itemChanged(event) {
    this.isactive = parseInt(event.target.text)
  
   
    this.Value.emit(this.isactive)

  }
  prev() {
    if (this.isactive != 1) {
      this.isactive -= 1
      this.Value.emit(this.isactive)
    }
  }
  next() {
    if (this.isactive != this.no_of_pages) {
      this.isactive += 1;
      this.Value.emit(this.isactive)  
    }
  }
  first(){
      this.isactive = 1;
      this.Value.emit(this.isactive) 
  }
  last(){
    this.isactive = this.no_of_pages;
    this.Value.emit(this.isactive)
}
}
