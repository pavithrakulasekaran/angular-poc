import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { ProjectComponent } from './project/project.component';
import { TaskComponent } from './task/task.component';
import { HomeComponent } from './home/home.component';



const routes: Routes = [
  {path:"home", component:HomeComponent},
  {path:'users', component:UsersComponent},
  {path:"project", component:ProjectComponent},
  {path:"task", component:TaskComponent},

  { path: '',
  redirectTo: '/home',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
