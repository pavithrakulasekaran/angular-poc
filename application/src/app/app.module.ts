import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule, NgbModal, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProjectComponent } from './project/project.component';
import { TaskComponent } from './task/task.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CalendarComponent } from './calendar/calendar.component';

import {  UserserviceService } from 'src/shared/services/userService.service'
import {HttpClientModule} from '@angular/common/http';
import { FilterPipe } from './filter.pipe';
import { SortingUserPipe } from './sorting-user.pipe';
import { PaginationComponent } from './pagination/pagination.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProjectComponent,
    TaskComponent,
    HomeComponent,
    CalendarComponent,
    FilterPipe,
    SortingUserPipe,
    PaginationComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,

    
  ],
  exports: [
    SortingUserPipe,
    FilterPipe
  ],
  providers: [NgbModal,NgbDatepickerConfig,UserserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
