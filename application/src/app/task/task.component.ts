import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskserviceService } from 'src/shared/services/taskservice.service';
import { task } from 'src/shared/model/task';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  task:task=new task();
  TaskValues:task[]=[];
  taskForm:FormGroup;
  constructor(private modalService: NgbModal,private fb:FormBuilder,
              private taskservice: TaskserviceService) { }

  ngOnInit() {
    this.taskForm=this.fb.group({
     taskName:['',[Validators.required,Validators.minLength(3)]],
      description:['',[Validators.required,Validators.minLength(10)]],
      status:[null,[Validators.required]],
      notes:['',[Validators.required,Validators.minLength(20)]],
   
  })
  this.taskservice.GetTasks().subscribe(
    data=>this.TaskValues=data)
  }
  AddTaskModel(content) {
    this.modalService.open(content);
  }
 
save(){
  window.location.reload();
  if(this.task.id==undefined){

    this.taskservice.SaveTask(this.task).subscribe(
      data=> this.TaskValues.push(data)
    )}
else{
   this.taskservice.UpdateTask(this.task).subscribe(
    data=>{
      this.TaskValues.splice(this.task.id,1,data)
    console.log(data);
    
    }
)
}
 
    }
    edit(content,task){
      this.modalService.open(content);
      this.taskservice.GetTaskByTaskId(task.id).subscribe(
        data=>{this.task=data
        })
     
      
    }
    DeleteTask(task){
  
      this.taskservice.Deletetask(task).subscribe(
        ret =>{ this.TaskValues.splice(task.id-1,1)}
      )
     }

}
