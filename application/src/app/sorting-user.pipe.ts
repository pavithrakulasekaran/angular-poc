import { Pipe, PipeTransform } from '@angular/core';
import { user } from 'src/shared/model/user';

@Pipe({
  name: 'sortingUser'
})
export class SortingUserPipe implements PipeTransform {

  transform(item:user[],field:string[],order:number): any {
    
    if(!item|| !field){
      return item;
    }
    return item.sort((a:user,b:user)=>{
    field.forEach(element => {
      
     a=a[element];
    b=b[element];

  })

    return a > b ? order * (- 1) :order ;
    
})
  }
  
}
