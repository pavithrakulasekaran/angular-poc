import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[],  value: string): any[] {
    if (!items) {
      return [];
    }
    if ( !value) {
      return items;
    }

    return items.filter(singleItem =>
      Object.keys(singleItem).some(k => 
        singleItem[k].toString().toLowerCase()
        .includes(value.toLowerCase()))
        );
        
    
  }

}
