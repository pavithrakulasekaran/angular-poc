import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { Observable } from 'rxjs';

import { user } from 'src/shared/model/user'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  employee:user[];
  public url="http://localhost:3000/users"

  constructor(private httpclient:HttpClient) { }

  SaveUser(user:user): Observable<user> {
    return this.httpclient.post<user>(this.url,user,httpOptions);   
  }
GetUsers(): Observable<user[]> {
  
 return this.httpclient.get<user[]>(this.url)
}
GetUser(id): Observable<user> {
  let user= this.httpclient.get<user>(`${this.url}/${id}`);
  console.log(user);
  return user
}
UpdateUser(user:user):Observable<user> {
  return this.httpclient.put<user>(`${this.url}/${user.id}`,user,httpOptions);
}

DeleteUser(user:user): Observable<user[]>{
  const tempUrl=`${this.url}/${user.id}`;
  return this.httpclient.delete<user[]>(tempUrl)
}
}
