import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { Observable } from 'rxjs';
import { task } from 'src/shared/model/task';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class TaskserviceService {
  public url="http://localhost:3000/tasks"
  constructor(private httpclient:HttpClient) { }
  SaveTask(Task:task): Observable<task> {
    return this.httpclient.post<task>(this.url,Task,httpOptions);   
  }

GetTasks(): Observable<task[]>{
  return this.httpclient.get<task[]>(this.url); 
}
UpdateTask(Task:task):Observable<task> {
  return this.httpclient.put<task>(`${this.url}/${Task.id}`,Task,httpOptions);
}

Deletetask(Task:task): Observable<task[]>{
  const tempUrl=`${this.url}/${Task.id}`;
  return this.httpclient.delete<task[]>(tempUrl)
}
SaveTaskNote(TaskNote){

}
GetTaskByTaskId(id):Observable<task> {
  {
    let task= this.httpclient.get<task>(`${this.url}/${id}`);
    console.log(task);
    return task
  }
}

 
}
